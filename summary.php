<html>
    <head>
        <title>Summary</title>
        <link rel="stylesheet" type="text/css" href="index.css"/>
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap" rel="stylesheet"/>
    </head>
    <body>
    <?php
        $donut = $_POST['donut'];
        $chocolate_cake = $_POST['chocolate_cake'];
        $wheat_bread = $_POST['wheat_bread'];
        $cherry_pie = $_POST['cherry_pie'];
        $cheesecake = $_POST['cheesecake'];
        $payment = $_POST['payment_method'];
        $payment_method;

        $donutPrice = $donut*0.25;
        $chocolate_cakePrice = $chocolate_cake*2.5;
        $wheat_breadPrice = $wheat_bread*2;
        $cherry_piePrice = $cherry_pie*2;
        $cheesecakePrice = ($cheesecake*2.3);

        $delivery_price = 2;

        $price = $donutPrice+$chocolate_cakePrice+$wheat_breadPrice+$cherry_piePrice+$cheesecakePrice;
        //$price = ($donut*0.25)+($chocolate_cake*2.5)+($wheat_bread*2)+($cherry_pie*2)+($cheesecake*2.3);
        $full_price = $delivery_price+$price;


        if($payment==1){
            $payment_method = "Online";
        }
        if($payment==2){
            $payment_method = "On delivery";
        }



echo '<div id="summary">';
echo '<div id="checkout">';
echo '<img style="float: right; width: 10%; margin: 1vh;" src="img/symbol4.svg"/>';
echo '<h2 style="margin-top: 0;">Checkout</h2>';

if($donut>0){
    echo<<<END
    <div id="ordered_product">
    <b>Donuts</b><span style="float:right">$$donutPrice</span><br/>
    Quantity: $donut<br/>
    </div>
END;
}
if($chocolate_cake>0){
    echo<<<END
    <div id="ordered_product">
    <b>Chocolate Cake</b> <i>slice</i><span style="float:right">$$chocolate_cakePrice</span><br/>
    Quantity: $chocolate_cake<br/>
    </div>
END;
}
if($wheat_bread>0){
    echo<<<END
    <div id="ordered_product">
    <b>Wheat Bread</b><span style="float:right">$$wheat_breadPrice</span><br/>
    Quantity: $wheat_bread<br/>
    </div>
END;
}
if($cherry_pie>0){
    echo<<<END
    <div id="ordered_product">
    <b>Cherry Pie</b> <i>slice</i><span style="float:right">$$cherry_piePrice</span><br/>
    Quantity: $cherry_pie<br/>
    </div>
END;
}
if($cheesecake>0){
    echo<<<END
    <div id="ordered_product">
    <b>Cheesecake</b> <i>slice</i><span style="float:right">$$cheesecakePrice</span><br/>
    Quantity: $cheesecake<br/>
    </div>
END;
}
    echo<<<END
    <br/>
    <b>Delivery price: $$delivery_price</b>
    <hr class="line"/>
    <h3>Total in: $$full_price</h3>
END;
echo '<b>'.'Payment: '.'</b>'.$payment_method;
echo '</div>';
echo '<button class="payment_button">Proceed to payment</button>';
echo '</div>';



?>
<script src="index.js"></script>
    </body>
</html>