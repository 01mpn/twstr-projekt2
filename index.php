<html>
    <head>
        <title>Place your order</title>
        <link rel="stylesheet" type="text/css" href="index.css"/>
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap" rel="stylesheet"/>
    </head>
    <body>
        <form id="order"action="summary.php" method="post">
            <div id="order_products">
                <div class="product">
                    <img src="img/donut.svg"/>
                    <h4>Donut</h4>
                        <input type="number" name="donut" value="0" min="0"/>
                        </div>
                <div class="product">
                    <img src="img/cake.svg"/>
                    <h4>Chocolate Cake<i> slice</i></h4>
                        <input type="number" name="chocolate_cake" value="0" min="0" max="20"/>
                        </div>
                <div class="product">
                    <img src="img/bread.svg"/>
                    <h4>Wheat Bread</h4>
                        <input type="number" name="wheat_bread" value="0" min="0" max="20"/>
                        </div>
                <div class="product">
                    <img src="img/pie.svg"/>
                    <h4>Cherry Pie<i> slice</i></h4>
                        <input type="number" name="cherry_pie" value="0" min="0" max="20"/>
                        </div>
                <div class="product">
                    <img src="img/cake.svg"/>
                    <h4>Cheesecake<i> slice</i></h4>
                        <input type="number" name="cheesecake" value="0" min="0" max="20"/>
                        </div>
                    </div>
            <div id="order_message">
                <img src="img/logo5.svg"/>
                    </div>
            <div id="order_payment">
                <h2>Choose payment method</h2>
                    <div id="method">
                        <label><input type="radio" name="payment_method" value="1"/><img src="img/credit-card.svg"/></label>
                        <label><input type="radio" name="payment_method" value="2"/><img src="img/cash.svg"/></label>
                        </div>
                    <button id="send_button" type="submit">Submit your order</button>
                    </div>
        </form>
        <script src="index.js"></script>
    </body>
</html>